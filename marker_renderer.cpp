#include "marker_renderer.h"
#define TINYOBJLOADER_IMPLEMENTATION
#include "tiny_obj_loader.h"

const static std::vector<std::string> marker_texture_files = {
	"markers/dot.png",
	"markers/open_circle.png",
	"markers/cross.png",
	"markers/plus.png",
	"markers/open_up_triangle.png",
	"markers/open_down_triangle.png",
	"markers/asterisk.png",
	"markers/open_diamond.png",
	"markers/filled_star.png",
	"markers/large_filled_circle.png",
	"markers/filled_up_triangle.png",
	"markers/filled_down_triangle.png",
	"markers/filled_square.png",
	"markers/filled_diamond.png",
	"markers/circle_dot.png",
	"markers/square_crosshair.png",
	"markers/round_crosshair.png",
	"markers/unknown.png"
};
// -1 indicates we don't have the texture for that glyph
const static std::unordered_map<std::string, int> marker_id_map = {
	{"Dot", 0},
	{"OpenCircle", 1},
	{"Cross", 2},
	{"Plus", 3},
	{"OpenUpTriangle", 4},
	{"OpenDownTriangle", 5},
	{"OpenSquare", -1},
	{"Asterisk", 6},
	{"OpenDiamond", 7},
	{"FilledStar", 8},
	{"FilledCircle", 9},
	{"FilledUpTriangle", 10},
	{"FilledDownTriangle", 11},
	{"FilledSquare", 12},
	{"FilledDiamond", 13},
	{"Flower", -1},
	{"OpenStar", -1},
	{"DoubleCircle", 14},
	{"Circle1", -1},
	{"Circle2", -1},
	{"Circle3", -1},
	{"Circle4", -1},
	{"Circle5", -1},
	{"Circle6", -1},
	{"Circle7", -1},
	{"Circle8", -1},
	{"Circle9", -1},
	{"Flower2", -1},
	{"SnowFlake", -1},
	{"OpenFinial", -1},
	{"FilledFinial", -1},
	{"MalteseCross", -1},
	{"FilledQuadStar", -1},
	{"OpenQuadStar", -1},
	{"Flower3", -1},
	{"Pinwheel", -1},
	{"TexacoStar", -1},
	{"ShadedStar", -1},
	{"SkiBasket", -1},
	{"Clock", -1},
	{"ThinArrow", -1},
	{"ThickArrow", -1},
	{"SquareGunSight", 15},
	{"GunSight", 16},
	{"TriStar", -1},
	{"NinjaStar", -1},
	{"KnightsCross", -1},
	{"Splat", -1},
	{"CircleArrow", -1},
	{"CircleCross", -1},
	{"OpenUpTriangle", -1},
	{"OpenCircle", -1},
	{"Cross", -1},
	{"OpenDiamond", -1}
};
static int get_marker_id(const std::string &type) {
	auto mid_fnd = marker_id_map.find(type);
	if (mid_fnd == marker_id_map.end() || mid_fnd->second == -1) {
		return marker_texture_files.size() - 1;
	} else {
		return mid_fnd->second;
	}
}

MarkerRenderer::MarkerRenderer(glt::BufferAllocator &allocator)
	: vao(0), shader(0), texture_array(0), num_vertices(0), num_markers(0)
{
	const std::string resource_path = glt::get_resource_path();
	const std::string cube_model_file = resource_path + "marker_cube.obj";
	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;
	std::string obj_load_err;
	if (!tinyobj::LoadObj(&attrib, &shapes, &materials, &obj_load_err, cube_model_file.c_str())) {
		std::cout << "TinyOBJ load error: " << obj_load_err << std::endl;
		throw std::runtime_error(obj_load_err);
	}
	if (shapes.size() != 1) {
		std::cout << "It seems we may not have the right marker cube model!?\n";
	}
	shader = glt::load_program({std::make_pair(GL_VERTEX_SHADER, resource_path + "nlxml_marker_vert.glsl"),
			std::make_pair(GL_FRAGMENT_SHADER, resource_path + "nlxml_marker_frag.glsl")});

	for (const auto &fv : shapes[0].mesh.num_face_vertices) {
		num_vertices += static_cast<size_t>(fv);
	}

	vbo = allocator.alloc((sizeof(glm::vec3) + sizeof(glm::vec3) + sizeof(glm::vec2)) * num_vertices,
			sizeof(GLfloat));
	{
		float *verts = static_cast<float*>(vbo.map(GL_ARRAY_BUFFER, GL_MAP_WRITE_BIT));
		size_t i = 0;
		for (const auto &fv : shapes[0].mesh.num_face_vertices) {
			for (size_t v = 0; v < fv; ++v, ++i) {
				tinyobj::index_t idx = shapes[0].mesh.indices[i];
				// Write positions
				for (size_t k = 0; k < 3; ++k) {
					verts[8 * i + k] = attrib.vertices[3 * idx.vertex_index + k];
				}
				// Write normals
				if (idx.normal_index != -1) {
					for (size_t k = 0; k < 3; ++k) {
						verts[8 * i + 3 + k] = attrib.normals[3 * idx.normal_index + k];
					}
				}
				// Write texcoords
				if (idx.texcoord_index != -1) {
					for (size_t k = 0; k < 2; ++k) {
						verts[8 * i + 6 + k] = attrib.texcoords[2 * idx.texcoord_index + k];
					}
				}
			}
		}
		vbo.unmap(GL_ARRAY_BUFFER);
	}

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo.buffer);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat),
			(void*)vbo.offset);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat),
			(void*)(vbo.offset + 3 * sizeof(GLfloat)));
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat),
			(void*)(vbo.offset + 6 * sizeof(GLfloat)));
	glBindVertexArray(0);

	glActiveTexture(GL_TEXTURE3);
	std::vector<std::string> marker_resources = marker_texture_files;
	for (auto &s : marker_resources) {
		s = resource_path + s;
	}
	texture_array = glt::load_texture_array(marker_resources);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glUseProgram(shader);
	glUniform1i(glGetUniformLocation(shader, "marker_textures"), 3);
	glActiveTexture(GL_TEXTURE0);
}
void MarkerRenderer::add_marker(const nlxml::Marker &m, const glm::mat4 &n_mat) {
	const int mid = get_marker_id(m.type);
	for (const auto &p : m.points) {
		const glm::vec4 pos = n_mat * glm::vec4(p.x, p.y, p.z, 1.0);
		new_markers.push_back(glm::vec4(pos.x, pos.y, pos.z, mid));
		new_markers.push_back(glm::vec4(m.color.r, m.color.g, m.color.b, 1.0));
	}
}
void MarkerRenderer::render(glt::BufferAllocator &allocator) {
	// Upload any new markers we've loaded
	if (!new_markers.empty()) {
		if (num_markers == 0) {
			marker_data = allocator.alloc(std::max(new_markers.size(), size_t{1024}) * sizeof(glm::vec4),
					glt::BufAlignment::SHADER_STORAGE_BUFFER);
		} else if (num_markers == marker_data.size / (2 * sizeof(glm::vec4))) {
			allocator.realloc(marker_data, marker_data.size * 2,
					glt::BufAlignment::SHADER_STORAGE_BUFFER);
		}
		glm::vec4 *mdata = static_cast<glm::vec4*>(marker_data.map(GL_SHADER_STORAGE_BUFFER,
					GL_MAP_WRITE_BIT));
		mdata += num_markers * 2;
		for (size_t i = 0; i < new_markers.size(); ++i) {
			mdata[i] = new_markers[i];
		}
		marker_data.unmap(GL_SHADER_STORAGE_BUFFER);
		num_markers += new_markers.size() / 2;
		std::cout << "Loaded new markers, now have " << num_markers << "\n";
		new_markers.clear();
	}

	// Draw the markers
	if (num_markers > 0) {
		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_2D_ARRAY, texture_array);
		glBindVertexArray(vao);
		glUseProgram(shader);
		glBindBufferRange(GL_SHADER_STORAGE_BUFFER, 2, marker_data.buffer, marker_data.offset, marker_data.size);

		glDrawArraysInstanced(GL_TRIANGLES, 0, num_vertices, num_markers);

		glActiveTexture(GL_TEXTURE0);
		glBindVertexArray(0);
	}
}

