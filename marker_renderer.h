#pragma once

#include <memory>
#include <iostream>
#include <vector>
#include <unordered_map>

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <imgui.h>

#include <vl/glt/buffer_allocator.h>
#include <vl/plugin.h>
#include <vl/glt/util.h>
#include "src/nlxml.h"

class MarkerRenderer {
	// TODO: Everything but the marker_data buffer can be
	// shared among all marker renderers
	GLuint vao, texture_array;
	size_t num_vertices, num_markers;
	glt::SubBuffer vbo, marker_data;
	std::vector<glm::vec4> new_markers;

public:
	MarkerRenderer(glt::BufferAllocator &allocator);
	void add_marker(const nlxml::Marker &m, const glm::mat4 &n_mat);
	void render(glt::BufferAllocator &allocator);

	GLuint shader;
};

