#include <memory>
#include <stack>
#include <iostream>
#include <utility>
#include <vector>
#include <unordered_map>

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <imgui.h>

#include <vl/glt/buffer_allocator.h>
#include <vl/plugin.h>
#include <vl/glt/util.h>
#include "src/nlxml.h"
#include "marker_renderer.h"

struct RenderLines {
	bool closed;
	glm::vec4 color;
	size_t count;
	glt::SubBuffer vbo;
};
struct NeuronRenderData {
	std::string file;
	nlxml::NeuronData data;
	std::vector<RenderLines> render_data;
	std::unique_ptr<MarkerRenderer> marker_renderer;
	bool display;
	bool rendered;

	NeuronRenderData(const std::string &file)
		: file(file), data(nlxml::import_file(file)), marker_renderer(nullptr),
		display(true), rendered(false)
	{}
};

static GLuint dummy_vao = 0;
static GLuint neuron_shader = 0;
static GLuint line_color_uniform = 0;
static std::vector<NeuronRenderData> neuron_data;

static void ui_fn(){
	if (neuron_data.empty()) {
		return;
	}
	if (ImGui::Begin("Neuron Info")) {
		size_t nid = 0;
		for (auto &n : neuron_data) {
			ImGui::PushID(nid++);
			ImGui::Text("File: %s\n# Trees: %d\n# Contours %d\n# Top-level Markers %d",
					n.file.c_str(), static_cast<int>(n.data.trees.size()),
					static_cast<int>(n.data.contours.size()), static_cast<int>(n.data.markers.size()));
			ImGui::Checkbox("Display", &n.display);
			if (!n.render_data.empty()) {
				float color[3] = {n.render_data[0].color.x, n.render_data[0].color.y, n.render_data[0].color.z};
				ImGui::ColorEdit3("Color ", color);
				for (auto &r : n.render_data) {
					r.color = glm::vec4(color[0], color[1], color[2], 1.f);
				}
			}
			ImGui::Separator();
			ImGui::PopID();
		}
	}
	ImGui::End();
}
static void upload_branch(const nlxml::Branch &b, const nlxml::Point &branch_from,
		const glm::vec4 &color, std::vector<RenderLines> &list, MarkerRenderer &marker_renderer,
		glt::BufferAllocator &allocator, const glm::mat4 &n_mat)
{
	RenderLines rl;
	rl.closed = false;
	rl.color = color;
	rl.count = b.points.size() + 1;
	rl.vbo = allocator.alloc(sizeof(glm::vec3) * (b.points.size() + 1));
	{
		glm::vec3 *pos = reinterpret_cast<glm::vec3*>(rl.vbo.map(GL_ARRAY_BUFFER, GL_MAP_WRITE_BIT));
		pos[0] = glm::vec3(n_mat * glm::vec4(branch_from.x, branch_from.y, branch_from.z, 1.0));
		for (size_t i = 0; i < b.points.size(); ++i) {
			pos[i + 1] = glm::vec3(n_mat * glm::vec4(b.points[i].x, b.points[i].y, b.points[i].z, 1.0));
		}
		rl.vbo.unmap(GL_ARRAY_BUFFER);
	}
	list.push_back(rl);

	for (const auto &m : b.markers) {
		marker_renderer.add_marker(m, n_mat);
	}
}
static void render_fn(std::shared_ptr<glt::BufferAllocator> &allocator, const glm::mat4 &view,
		const glm::mat4 &proj, const float elapsed)
{
	if (dummy_vao == 0) {
		glGenVertexArrays(1, &dummy_vao);
	}
	if (neuron_shader == 0) {
		const std::string resource_path = glt::get_resource_path();
		neuron_shader = glt::load_program({std::make_pair(GL_VERTEX_SHADER, resource_path + "nlxml_vert.glsl"),
				std::make_pair(GL_FRAGMENT_SHADER, resource_path + "nlxml_frag.glsl")});
		line_color_uniform = glGetUniformLocation(neuron_shader, "tree_color");
	}

	for (auto &n : neuron_data) {
		if (!n.marker_renderer) {
			n.marker_renderer = std::make_unique<MarkerRenderer>(*allocator);
		}
		if (!n.rendered) {
			glm::mat4 n_mat(1);
			if (!n.data.images.empty()) {
				/*
				const glm::vec3 t = glm::vec3(n.data.images[0].coord[0],
						n.data.images[0].coord[1], n.data.images[0].coord[2]);
				const glm::vec3 s = glm::vec3(n.data.images[0].scale[0], n.data.images[0].scale[1],
						n.data.images[0].z_spacing);
				n_mat = glm::translate(t) * glm::scale(s);
				*/
			}
			// TODO: Don't hardcode this, these are just valid for the eval files we're producing
			const glm::vec3 t = glm::vec3(0, -14, -21);
			const glm::vec3 s = glm::vec3(1, -1, 1);
			n_mat = glm::translate(t) * glm::scale(s);
			n.rendered = true;
			for (const auto &t : n.data.trees) {
				RenderLines rl;
				rl.closed = false;
				rl.color = glm::vec4(t.color.r, t.color.g, t.color.b, 1.0);
				rl.count = t.points.size();
				rl.vbo = allocator->alloc(sizeof(glm::vec3) * t.points.size());
				{
					glm::vec3 *pos = reinterpret_cast<glm::vec3*>(rl.vbo.map(GL_ARRAY_BUFFER, GL_MAP_WRITE_BIT));
					for (size_t i = 0; i < t.points.size(); ++i) {
						pos[i] = glm::vec3(n_mat * glm::vec4(t.points[i].x, t.points[i].y, t.points[i].z, 1.0));
					}
					rl.vbo.unmap(GL_ARRAY_BUFFER);
				}
				n.render_data.push_back(rl);
				for (const auto &m : t.markers) {
					n.marker_renderer->add_marker(m, n_mat);
				}
				// Recursively upload all the branches
				using UploadBranch = std::pair<const nlxml::Branch*, const nlxml::Point>;
				std::stack<UploadBranch> branches;
				for (const auto &b : t.branches) {
					branches.push(std::make_pair(&b, t.points.back()));
				}
				while (!branches.empty()) {
					UploadBranch b = branches.top();
					upload_branch(*b.first, b.second, rl.color, n.render_data, *n.marker_renderer,
							*allocator, n_mat);

					nlxml::Point b_from;
					if (!b.first->points.empty()) {
						b_from = b.first->points.back();
					} else {
						b_from = b.second;
					}
					branches.pop();
					// Push children on the stack
					for (const auto &sb : b.first->branches) {
						branches.push(std::make_pair(&sb, b_from));
					}
				}
			}
			for (const auto &c : n.data.contours) {
				RenderLines rl;
				rl.closed = c.closed;
				rl.color = glm::vec4(c.color.r, c.color.g, c.color.b, 1.0);
				rl.count = c.points.size();
				rl.vbo = allocator->alloc(sizeof(glm::vec3) * c.points.size());
				{
					glm::vec3 *pos = reinterpret_cast<glm::vec3*>(rl.vbo.map(GL_ARRAY_BUFFER, GL_MAP_WRITE_BIT));
					for (size_t i = 0; i < c.points.size(); ++i) {
						pos[i] = glm::vec3(n_mat * glm::vec4(c.points[i].x, c.points[i].y, c.points[i].z, 1.0));
					}
					rl.vbo.unmap(GL_ARRAY_BUFFER);
				}
				n.render_data.push_back(rl);
				for (const auto &m : c.markers) {
					n.marker_renderer->add_marker(m, n_mat);
				}
			}
			for (const auto &m : n.data.markers) {
				n.marker_renderer->add_marker(m, n_mat);
			}
		}

		if (n.display) {
			glUseProgram(neuron_shader);
			glBindVertexArray(dummy_vao);
			glEnableVertexAttribArray(0);
			for (const auto &r : n.render_data) {
				glBindBuffer(GL_ARRAY_BUFFER, r.vbo.buffer);
				glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)r.vbo.offset);
				glUniform4fv(line_color_uniform, 1, glm::value_ptr(r.color));
				if (!r.closed) {
					glDrawArrays(GL_LINE_STRIP, 0, r.count);
				} else {
					glDrawArrays(GL_LINE_LOOP, 0, r.count);
				}
			}
			glUseProgram(0);
			glBindVertexArray(0);
			n.marker_renderer->render(*allocator);
		}
	}
}
static bool loader_fn(const vl::FileName &file_name){
	std::cout << "nlxml plugin loading file: " << file_name << "\n";
	neuron_data.push_back(NeuronRenderData(file_name.file_name));
	return true;
}
static bool vislight_plugin_nlxml_init(const std::vector<std::string> &args, vl::PluginFunctionTable &fcns,
		vl::MessageDispatcher &dispatcher)
{
	std::cout << "nlxml plugin loaded, args = {\n";
	for (const auto &a : args){
		std::cout << "\t" << a << "\n";
		vl::FileName fname = a;
		if (fname.extension() == "xml") {
			loader_fn(fname);
		}
	}
	std::cout << "}\n";

	fcns.plugin_type = vl::PluginType::UI_ELEMENT | vl::PluginType::RENDER | vl::PluginType::LOADER;
	fcns.ui_fn = ui_fn;
	fcns.render_fn = render_fn;
	fcns.loader_fn = loader_fn;
	fcns.file_extensions = "xml";
	return true;
}
static void unload_fn(){}

// Setup externally callable loaders so the plugin loader can find our stuff
PLUGIN_LOAD_FN(nlxml)
PLUGIN_UNLOAD_FN(nlxml, unload_fn)

