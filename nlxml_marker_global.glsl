struct MarkerAttrib {
	// The marker info is: x, y, z, type_id
	vec4 info;
	vec4 color;
};

layout(std430, binding = 2) buffer MarkerData {
	MarkerAttrib marker_info[];
};

