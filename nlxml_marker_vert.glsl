#version 430 core

// For the nlxml vislight plugin you need to copy these
// shaders into the resource directory

#include "view_info.glsl"
#include "nlxml_marker_global.glsl"

layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uv;

out vec2 vuv;
out flat int marker_id;
out flat vec4 mcolor;

void main(void) {
	vuv = uv;
	MarkerAttrib mdata = marker_info[gl_InstanceID];
	marker_id = int(mdata.info.w);
	mcolor = mdata.color;
	gl_Position = proj * view * vec4(pos / 100 + mdata.info.xyz / 100, 1);
}


