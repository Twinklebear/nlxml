#version 430 core

uniform sampler2DArray marker_textures;

in vec2 vuv;
in flat int marker_id;
in flat vec4 mcolor;

out vec4 color;

void main(void) {
	vec3 tex = (vec4(1) - texture(marker_textures, vec3(vuv, marker_id))).rgb;
	if (tex.r == 0.0) {
		color = vec4(0.4);
	} else {
		color.xyz = mcolor.xyz * tex;
		color.a = max(tex.r, max(tex.g, tex.b));
	}
}

