#version 430 core

uniform vec4 tree_color;

out vec4 color;

void main(void) {
	color = tree_color;
}

