#version 430 core

// For the nlxml vislight plugin you need to copy these
// shaders into the resource directory

#include "view_info.glsl"

layout(location = 0) in vec3 pos;

void main(void) {
	gl_Position = proj * view * vec4(pos / 100, 1);
}

